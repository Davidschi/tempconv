def celsius_to_kelvin(celsius):
    return celsius + 273.15


def kelvin_to_celsius(kelvin):
    return kelvin - 273.15


def celsius_to_fahrenheit(celsius):
    return celsius * 1.8 + 32


def kelvin_to_fahrenheit(kelvin):
    return kelvin * 1.8 - 459.67


def fahrenheit_to_celsius(fahrenheit):
    return (fahrenheit - 32) * 5 / 9


def fahrenheit_to_kelvin(fahrenheit):
    return (fahrenheit - 32) / 1.8 + 273.15


def type_select():
    while True:
        n = input(
            'Please put in the corresponding number of what you want to do.\n1 Convert Celsius to Kelvin\n2 Convert Kelvin to Celsius\n3 Convert Celsius to Fahrenheit\n4 Convert Kelvin to Fahrenheit\n5 Convert Fahrenheit to Celsius\n6 Convert Fahrenheit to Kelvin\n'
        )
        if n in ('1', '2', '3', '4', '5', '6'):
            try:
                n = int(n)
                return n
            except ValueError:
                print('Sorry, please put in a number from 1 to 6.')
        elif n not in ('1', '2', '3', '4', '5', '6'):
            print('I\'m sorry, but there\'s no function ' + str(n) + '.')


def temp_get(n):
    units = {
        1: 'Celsius',
        2: 'Kelvin',
        3: 'Celsius',
        4: 'Kelvin',
        5: 'Fahrenheit',
        6: 'Fahrenheit'
    }
    while True:
        t = input('Please put in the temperature in ' + units[n] + ': ')
        try:
            t = float(t)
            if n == 1:
                t = celsius_to_kelvin(t)
            elif n == 2:
                t = kelvin_to_celsius(t)
            elif n == 3:
                t = celsius_to_fahrenheit(t)
            elif n == 4:
                t = kelvin_to_fahrenheit(t)
            elif n == 5:
                t = fahrenheit_to_celsius(t)
            elif n == 6:
                t = fahrenheit_to_kelvin(t)
            return t
        except ValueError:
            print('Sorry, this is not a valid number.')


if __name__ == '__main__':
    print(temp_get(type_select()))
